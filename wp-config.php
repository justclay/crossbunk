<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', '' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J26tV8EyIGGwgiAPUP8zowr4YSHhJVb2cOmvegdhLoYvKk2ouu9ap6mO29o4XQm9B7D0jZOAOGEqooJKD96IYA==');
define('SECURE_AUTH_KEY',  'klh6ULDPb9QZ+qsLfnbFIlaJjL26qOuSIbe5cisDn9gkEKIbXHlVlKsDRC9ZNPw8T62RPFailPXZurKJduOSLQ==');
define('LOGGED_IN_KEY',    'D5su/JMKLsIsFAiPWKO0gOGh/cSyL0k6rck2VLYeRFrxaw0vOJAuf64DQt9X4VitKtSQIDNpKgBDigcK8kcaZg==');
define('NONCE_KEY',        's8Cxf5QoTH5uU5J1zkK9a9ef2sOijfSjQ2angnpxek5Ax7jbN73diBxe6q/5V3TyT/nJLa/RL5zXkS1HjJDGPA==');
define('AUTH_SALT',        'pHhtvAs/XKRtslkn6hqpOO/+9FLt1VwoF1LzXJl2jev3f4b3X+JHQWx7JqYZMFbkubOSXdOriIa/fXQJSkJ3Gg==');
define('SECURE_AUTH_SALT', '4g6tbtO7rCAUxB/DAVnrgp9B1U+8Ffn2P+zFQYn+/a+GqeL1YT+FIIVTUU+z2Dw42v7lsKupZ/ZIlzHPx8DCog==');
define('LOGGED_IN_SALT',   'wgngVA0lTUrE/5zE6eCABFbEU1iPQFu2FEITUiSYgGiJYyjUj4l217Fl4n/pRVyhKK7S3OvbGkofftalnVAXLg==');
define('NONCE_SALT',       'KOIbIWiOmSnTYx/xe3u8lyAm0wLpOAjcMR84/axWmmK4NU+8oPTdheSQf8KuVBGUqe/SQ7CmFuKIbHd1G6EiWQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
